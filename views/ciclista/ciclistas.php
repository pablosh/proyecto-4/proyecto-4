<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use circulon\widgets\ColumnListView;
$titulo = "Ciclistas de nuestro equipo";
?>

<div class="well well-sm separator"><h2 style="text-align: center; max-height: 80px"><?=$titulo?></h2></div>

<div class="">
    <?= ColumnListView::widget([ //Mostrar un los ciclistas en bloques de 4
        'dataProvider' => $dataProvider,
        'itemView' => '_ciclista',
        'layout'=> "{summary}\n{pager}\n{items}",
        'columns' => 2,
    ]);
    ?>
</div>