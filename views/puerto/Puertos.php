<?php
use yii\helpers\Html;
use yii\helpers\HTMLPurifier;
use circulon\widgets\ColumnListView;
$titulo = "Puertos que hemos ganado";
?>

<div class="well well-sm separator"><h2 style="text-align: center; max-height: 80px"><?=$titulo?></h2></div>

<div class=""> <!-- List view obtenido en https://github.com/circulon/yii2-columnlistview -->
    <?= ColumnListView::widget([ //Mostrar las puertos en bloques de 4
        'dataProvider' => $dataProvider,
        'itemView' => '_puerto',
        'layout' => "{summary}\n{pager}\n{items}",
        'columns'=> 2,
    ]);
    ?>
</div>
